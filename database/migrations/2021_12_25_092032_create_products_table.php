<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('idproduct')->nullable();
            $table->integer('idvatgroup')->nullable();
            $table->string('idsupplier')->nullable();
            $table->string('productcode')->nullable();
            $table->string('name')->nullable();
            $table->double('price')->nullable();
            $table->double('fixedstockprice')->nullable();
            $table->string('productcode_supplier')->nullable();
            $table->string('deliverytime')->nullable();
            $table->text('description')->nullable();
            $table->string('barcode')->nullable();
            $table->boolean('unlimitedstock')->nullable();
            $table->boolean('assembled')->nullable();
            $table->string('type')->nullable();
            $table->double('weight')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->integer('minimum_purchase_quantity')->nullable();
            $table->integer('purchase_in_quantities_of')->nullable();
            $table->string('hs_code')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('comment_count')->nullable();
            $table->string('analysis_abc_classification')->nullable();
            $table->string('analysis_pick_amount_per_day')->nullable();
            $table->text('tags')->nullable();
            $table->text('productfields')->nullable();
            $table->text('images')->nullable();
            $table->text('stock')->nullable();
            $table->text('pricelists')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
