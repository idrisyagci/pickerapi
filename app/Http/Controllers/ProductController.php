<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Product;

class ProductController extends Controller
{
    public function getAllProducts(){
        $products = Http::withBasicAuth('dev', '19@d3mg!')->get('https://fairweb.picqer.com/api/v1/products');
        $prod = json_decode($products, true);

        foreach($prod as $p){
            $p['tags'] = '';
            $p['productfields'] = '';
            $p['images'] = '';
            //$p['stock'] = '';
            $p['stock'] = count($p['stock']) > 0 ? $p['stock'][0]['stock'] : 0;
            $p['pricelists'] = '';
            Product::create($p);
        }
        return view('products', ['data'=>json_decode(json_encode($prod))]);
    }

    public function singleProduct(Request $request){
        $product = Product::where('idproduct', $request->id)->get();
        return view('product', ['data'=>$product[0]]);
    }

    public function editProduct(Request $request){
        $product = Product::where('idproduct', $request->id)->get();
        return view('edit', ['data'=>$product[0]]);
    }

    public function updateProduct(Request $request){
        $id = $request->idproduct;
        $data = [
            'name' => $request->name,
            'productcode' => $request->code,
            'price' => $request->price,
            'description' => $request->description
        ];

        $update = Http::withBasicAuth('dev', '19@d3mg!')->put('https://fairweb.picqer.com/api/v1/products/'.$id, $data);
        $product = json_decode($update);
        if(isset($product->idproduct)){
            $update = Product::where('idproduct', $id)->update($data);
            if ($update){
                $product->tags = '';
                $product->productfields = '';
                $product->images  = '';
                //$product->stock = '';
                $product->stock = count($product->stock) > 0 ? $product->stock[0]->stock : 0;
                $product->pricelists = '';
                return view('product', ['data' => $product]);
            }else{
                return redirect()->back()->with('status', 'Product did not update');
            }
        }
    }

    public function addProduct(Request $request){
        $data = [
            'idproduct' => $request->idproduct,
            'name' => $request->name,
            'productcode' => $request->code,
            'price' => $request->price,
            'description' => $request->description
        ];

        $add = Http::withBasicAuth('dev', '19@d3mg!')->post('https://fairweb.picqer.com/api/v1/products', $data);
        $p = json_decode($add, true);

        if(isset($p['idproduct'])){
            $p['tags'] = '';
            $p['productfields'] = '';
            $p['images'] = '';
            $p['stock'] = count($p['stock']) > 0 ? $p['stock'][0]['stock'] : 0;
            $p['pricelists'] = '';

            $add = Product::create($p);

            if ($add){
                return view('product', ['data' => json_decode(json_encode($p))]);
            }else{
                return redirect()->back()->with('status', 'Product did not update');
            }
        }else {
            return redirect()->back()->with('status', 'Something went wrong');
        }
    }

    public function editStock(Request $request){
        $id = $request->id;
        $data = [
            "change" =>  $request->stock,
            "reason" =>  $request->reason
        ];

        $update = Http::withBasicAuth('dev', '19@d3mg!')->post('https://fairweb.picqer.com/api/v1/products/'.$id.'/stock/6209', $data);
        $prod = json_decode($update);
        if(isset($prod->idwarehouse)){
          $update =  Product::where('idproduct', $id)->update(['stock' => $prod->stock]);
          if($update){
            $p = Product::where('idproduct', $id)->get();
            return view('product', ['data' => $p[0]]);
          }
        }
    }
}
