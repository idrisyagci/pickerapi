<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Models\Product;

Route::get('/', function () {
    if(Product::all()->count() > 0){
        $data = Product::all();
        return view('products', ['data'=>$data]);
    }else {
        return view('home');
    }
    
})->name('index');

Route::get('/products', function () {
    $data = Product::all();
    return view('products', ['data'=>$data]);
})->name('products');;

Route::get('/add-product', function () {
    return view('add');
})->name('add-product');

Route::get('/get-products', [ProductController::class, 'getAllProducts'])->name('get');
Route::get('/product/{id}', [ProductController::class, 'singleProduct'])->name('product');
Route::get('/product/edit/{id}', [ProductController::class, 'editProduct'])->name('edit');
Route::post('/product/update', [ProductController::class, 'updateProduct'])->name('update');
Route::post('/product/add', [ProductController::class, 'addProduct'])->name('add');
Route::post('/product/stock', [ProductController::class, 'editStock'])->name('stock-edit');