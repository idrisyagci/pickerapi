@include('header')
<!-- HOT DEAL SECTION -->
<div id="hot-deal" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<div class="hot-deal">
					<h2 class="text-uppercase">there is no product on the database</h2>
					<a class="primary-btn cta-btn" href="{{route('get')}}">Get All Products</a>
				</div>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /HOT DEAL SECTION -->
@include('footer')