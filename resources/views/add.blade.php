@include('header') 
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <div class="col-md-12">
                    <!-- Billing Details -->
                    <form method="post" class="form" action="{{route('add')}}">
                        @csrf
                        <div class="billing-details">
                            <div class="section-title">
                                <h3 class="title">Add Product</h3>
                            </div>
                            <div class="form-group">
                                <input class="input" type="text" name="idproduct" placeholder="Product Id">
                            </div>
                            <div class="form-group">
                                <input class="input" type="text" name="name" placeholder="Product Name">
                            </div>
                            <div class="form-group">
                                <input class="input" type="text" name="code" placeholder="Product Code">
                            </div>
                            <div class="form-group">
                                <input class="input" type="text" name="price" placeholder="Price">
                            </div>
                            <div class="form-group">
                                <textarea class="input" name="description" placeholder="Description"></textarea>
                            </div>

                        </div>
                        
                        <!-- /Billing Details -->
                        
                        <div class="text-center">
                            <button type="submit" class="btn-lg primary-btn btn-block order-submit">Add Product</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@include('footer')