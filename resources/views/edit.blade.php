@include('header') 
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- Product tab -->
                <div class="col-md-12">
                    <div id="product-tab">
                        <!-- product tab nav -->
                        <ul class="tab-nav">
                            <li class="active"><a data-toggle="tab" href="#tab1">Edit Product Details</a></li>
                            <li><a data-toggle="tab" href="#tab2">Edit Stock Count</a></li>
                        </ul>
                        <!-- /product tab nav -->

                        <!-- product tab content -->
                        <div class="tab-content">
                            <!-- tab1  -->
                            <div id="tab1" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Billing Details -->
                                        <form method="post" class="form" action="{{route('update')}}">
                                            @csrf
                                            <div class="billing-details">
                                                <div class="section-title">
                                                    <h3 class="title">Edit Product</h3>
                                                </div>
                                                <div class="form-group">
                                                    <input class="input" type="text" name="idproduct" value="{{$data->idproduct}}" placeholder="Product Id" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <input class="input" type="text" name="name" value="{{$data->name}}" placeholder="Product Name">
                                                </div>
                                                <div class="form-group">
                                                    <input class="input" type="text" name="code" value="{{$data->productcode}}" placeholder="Product Code">
                                                </div>
                                                <div class="form-group">
                                                    <input class="input" type="text" name="price" value="{{$data->price}}" placeholder="Price">
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="input" name="description" placeholder="Description">{{$data->description}}</textarea>
                                                </div>
                    
                                                <div class="text-center">
                                                    <button type="submit" class="btn-lg primary-btn btn-block order-submit">Edit Product</button>
                                                </div>
                                            </div>
                                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /tab1  -->

                            <!-- tab2  -->
                            <div id="tab2" class="tab-pane fade in">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="post" class="form" action="{{route('stock-edit')}}">
                                            @csrf
                                            <div class="billing-details">
                                                <div class="section-title">
                                                    <h3 class="title">Update Stock</h3>
                                                </div>
                                                <div class="form-group">
                                                    <label>Stock Count</label>
                                                    <input type="hidden" value="{{$data->idproduct}}" name="id">
                                                    <input class="input" type="text" name="stock" value="{{$data->stock > 0 ? $data->stock : 0}}" placeholder="Stock Count">
                                                </div>
                                                <div class="form-group">
                                                    <label>Reason for Stock Update</label>
                                                    <input class="input" type="text" name="reason" placeholder="Reason for Stock Update" required>
                                                </div>
                                                                
                                                <div class="text-center">
                                                    <button type="submit" class="btn-lg primary-btn btn-block order-submit">Update Product</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /tab2  -->
                        </div>
                        <!-- /product tab content  -->
                    </div>
                </div>
                <!-- /product tab -->

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@include('footer')